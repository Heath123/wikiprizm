---
revisions:
- author: Ahelper
  comment: libfxcg API
  timestamp: '2014-11-23T00:28:22Z'
title: Print_OS
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x01F9\
**Function signature:** void Print_OS(const char\* msg, int invers, int
zero2)

Displays the given message at the cursor position defined by
[Locate_OS]({{< ref "Syscalls/Locate_OS.md" >}}). This will move the
cursor's position to after the last character printed.\

## Parameters

-   **msg** - string to be displayed.
-   **invers** - See [PrintXY]({{< ref "Syscalls/PrintXY.md" >}})'s
    usage of ***int** display_mode*.
-   **zero2** - must be zero for this function to work (possibly this is
    similar to the behavior of the last parameter of
    [PrintMiniMini]({{< ref "Syscalls/PrintMiniMini.md" >}})).
