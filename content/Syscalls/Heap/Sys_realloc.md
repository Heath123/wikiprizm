---
revisions:
- author: Gbl08ma
  comment: "Created page with \u201C{{Syscall \\| name = sys_realloc \\| header\n\
    = fxcg/heap.h \\| index = 0x1F46 \\| signature = void\n*sys_realloc(void *p, int\
    \ sz) \\| synopsis = Changes the size of a\nmemory block allocat\u2026\u201D"
  timestamp: '2014-07-30T19:55:45Z'
title: Sys_realloc
---

## Synopsis

**Header:** fxcg/heap.h\
**Syscall index:** 0x1F46\
**Function signature:** void \*sys_realloc(void \*p, int sz)

Changes the size of a memory block allocated on the heap with
[sys_malloc]({{< ref "Syscalls/Heap/sys_malloc.md" >}}).\

## Parameters

-   **p** - pointer to existing allocated block.
-   **sz** - new size of the allocated block.\

## Returns

A pointer to the buffer, possibly different from the original, that has
the new size. A null pointer will be returned if **sz** is zero (the
memory block is freed), or if for some reason the buffer could not be
reallocated (and in such case, the original block **p** points to will
not have been modified).\

## Comments

The heap implementation on the Prizm appears to have some bugs; it also
appears to not be able to optimize heap allocation so that fragmentation
is avoided. See [this forum
thread](http://www.cemetech.net/forum/viewtopic.php?t=7539) for more
information and discussion.

The [stack]({{< ref "/OS_Information/Processes.md#stack" >}}) can provide a
bigger continuous RAM area than the heap (up to about 500 KiB, depending on
static RAM requirements, versus about 128 KiB). This, plus the limitations
described above, means that to get the most out of the memory available to
add-ins, one has to use non-standard (*"incorrect"*) practices such as
preferably using the stack instead of the heap.
