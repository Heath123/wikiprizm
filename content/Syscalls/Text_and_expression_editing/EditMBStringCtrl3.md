---
revisions:
- author: Gbl08ma
  comment: "Created page with \u201C{{Syscall \\| name = EditMBStringCtrl3 \\|\nheader\
    \ = fxcg/keyboard.h \\| index = 0x120B \\| signature = void\nEditMBStringCtrl3(unsigned\
    \ char*, int xposmax, void*, void*, void*,\nint, in\u2026\u201D"
  timestamp: '2014-07-31T15:38:28Z'
title: EditMBStringCtrl3
---

## Synopsis

**Header:** fxcg/keyboard.h\
**Syscall index:** 0x120B\
**Function signature:** void EditMBStringCtrl3(unsigned char\*, int
xposmax, void\*, void\*, void\*, int, int, int, int, int)

The exact function of this syscall is not yet known; it is most probably
an even more overloaded variant of
[EditMBStringCtrl]({{< ref "Syscalls/Text_and_expression_editing/EditMBStringCtrl.md" >}}).\

## Parameters

The meaning of the parameters is not known.
