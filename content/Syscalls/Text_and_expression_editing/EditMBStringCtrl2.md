---
revisions:
- author: Gbl08ma
  timestamp: '2014-08-04T12:08:12Z'
title: EditMBStringCtrl2
---

## Synopsis

**Header:** fxcg/keyboard.h\
**Syscall index:** 0x120A\
**Function signature:** void EditMBStringCtrl2(unsigned char\*
MB_string, int posmax, int\* start, int\* xpos, int\* key, int x, int y,
int enable_pos_to_clear, int pos_to_clear)

Edits a [multi-byte string]({{< ref "Multi-byte_strings.md" >}})
according to the specified control key (where the GetKey code is higher
or equal to 30000), updates the cursor position and displays the result
at the specified position. To be used in conjunction with
[DisplayMBString2]({{< ref "Syscalls/Text_and_expression_editing/DisplayMBString2.md" >}}).
This is a overloaded variant of
[EditMBStringCtrl]({{< ref "Syscalls/Text_and_expression_editing/EditMBStringCtrl.md" >}}).\

## Parameters

-   **MB_string** - pointer to multi-byte string to edit. Must not be
    constant, as this syscall may actually want to modify the string
    contents, for example, when the user presses DEL, cuts or pastes
    text (cut and paste operations are linked to control keys).
-   **posmax** - maximum size, in bytes, the string can take; the size
    of the buffer **MB_string** points to should be taken into account,
    to avoid buffer overflows (when pasting from the clipboard, for
    example).
-   **start** - pointer to integer that will receive the new display
    start position (see the
    [DisplayMBString2]({{< ref "Syscalls/Text_and_expression_editing/DisplayMBString2.md" >}})
    parameter with the same name).
-   **xpos** - pointer to integer that will receive the new cursor
    position (see the
    [DisplayMBString2]({{< ref "Syscalls/Text_and_expression_editing/DisplayMBString2.md" >}})
    parameter with the same name).
-   **key** - pointer to integer with the
    [GetKey]({{< ref "Syscalls/Keyboard/GetKey.md" >}}) code of the key
    pressed. The value of **key** might be changed, possibly for
    communication between text editing syscalls and GetKey.
-   **x** - horizontal "homescreen" coordinate (like
    [PrintXY]({{< ref "Syscalls/PrintXY.md" >}})) of the current text
    editing field.
-   **y** - vertical coordinate, in pixels, of the current text editing
    field.
-   **enable_pos_to_clear** - if 1, the parameter **pos_to_clear** is
    taken into account, else the line is cleared to the end of the
    screen.
-   **pos_to_clear** - sets the horizontal "homescreen" coordinate after
    which the screen will not be cleared (marks the end of the text
    input).

**x** and **y** should be set to the same values used with
[DisplayMBString2]({{< ref "Syscalls/Text_and_expression_editing/DisplayMBString2.md" >}}),
for graphical consistency.\

## Comments

This syscall is for when a control key is pressed during text editing.
If the key pressed is not a control key, or in other words, if the
GetKey code is lower than 30000, use
[EditMBStringChar]({{< ref "Syscalls/Text_and_expression_editing/EditMBStringChar.md" >}})
instead.
