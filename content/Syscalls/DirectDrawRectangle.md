---
revisions:
- author: Gbl08ma
  comment: "Created page with \u201C{{Syscall \\| name = DirectDrawRectangle \\|\n\
    index = 0x02AA \\| signature = void DirectDrawRectangle(int x1, int\ny1, int x2,\
    \ int y2, unsigned short color) \\| header =\nfxcg/display.h\u2026\u201D"
  timestamp: '2014-07-29T11:38:39Z'
title: DirectDrawRectangle
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x02AA\
**Function signature:** void DirectDrawRectangle(int x1, int y1, int x2,
int y2, unsigned short color)

Draws a rectangle to the display, bypassing VRAM. Allows for drawing in
the frame area.\

## Parameters

-   **x1** - horizontal coordinate of the top-left corner of the
    rectangle, 0 to 395.
-   **y1** - vertical coordinate of the top-left corner of the
    rectangle, 0 to 223.
-   **x2** - horizontal coordinate of the bottom-right corner of the
    rectangle, 0 to 395.
-   **y2** - vertical coordinate of the bottom-right corner of the
    rectangle, 0 to 223.
-   **color** - fill
    [color]({{< ref "Technical_Documentation/display.md" >}}) of the
    rectangle.\

## Comments

This syscall appears to have a small bug/constraint: only rectangles
where a certain (multiple of 2/4/8/16?) number of pixels needs to be
drawn, will be drawn perfectly. Otherwise, there will be pixels missing
at the bottom right.

If you only need to draw in the area accessible through the VRAM (i.e.,
not in the frame area), then using the VRAM will be preferable because
it's faster and doesn't have the bugs/constraints this syscall has.
