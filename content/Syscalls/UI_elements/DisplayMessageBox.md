---
revisions:
- author: Gbl08ma
  timestamp: '2014-11-18T22:03:46Z'
title: DisplayMessageBox
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x1E4B\
**Function signature:** void DisplayMessageBox(unsigned char\* message)

Displays a message box with the specified message.\

## Parameters

-   **message** - A pointer to a buffer (possibly `const` and/or
    `static`) containing the message to display.\

## Comments

Key handling is done by the syscall, which will return when the user
presses EXIT, AC/ON, QUIT or the left or right cursor keys.

The "Press:\[EXIT\]" message is not displayed automatically.
