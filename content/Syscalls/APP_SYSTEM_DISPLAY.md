---
revisions:
- author: Gbl08ma
  timestamp: '2014-07-29T10:07:07Z'
title: APP_SYSTEM_DISPLAY
---

## Synopsis

**Header:** fxcg/app.h\
**Syscall index:** 0x1E0A\
**Function signature:** void APP_SYSTEM_DISPLAY(int opt);

Opens the "Display Settings" screen that shows in the start-up wizard
and in the System menu (when F1 is pressed).\

## Parameters

-   **opt** - Controls whether the screen should behave like in the
    wizard (1), or like in the System menu (0).\

## Comments

Allows for setting the brightness value in a scale from 1 to 5. The
screen is non-functional in the "fx-CG10/20 Manager" emulator, even
though it still displays and returns as described below.

If the value of the parameter is zero, the syscall returns when the user
presses EXIT. If the value of the parameter is 1, it returns when the
user presses F6.
