---
revisions:
- author: Gbl08ma
  timestamp: '2014-07-29T11:23:43Z'
title: Bdisp_PutDisp_DD_stripe
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x0260\
**Function signature:** Bdisp_PutDisp_DD_stripe(int y1, int y2);

Similar to
[Bdisp_PutDisp_DD]({{< ref "Syscalls/Bdisp_PutDisp_DD.md" >}}), this
syscall puts a specified section of the VRAM on the screen.\

## Parameters

-   **y1** - First VRAM row to be copied.
-   **y2** - Last VRAM row to be copied.\

## Comments

This gives a speed advantage if only a limited range of rows has to be
updated. The syscall does not appear to work on the "fx-CG10/20 Manager"
emulator.
