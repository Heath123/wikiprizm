---
revisions:
- author: Gbl08ma
  comment: "Created page with \u201C{{Syscall \\| name = DefineStatusAreaFlags\n\\\
    | index = 0x02B8 \\| signature = int DefineStatusAreaFlags(int P1,\nint flags,\
    \ void\\* P3, void\\* P4) \\| header = fxcg/display.h \\|\nparameters\u2026\u201D"
  timestamp: '2014-07-29T18:27:17Z'
title: DefineStatusAreaFlags
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x02B8\
**Function signature:** int DefineStatusAreaFlags(int P1, int flags,
void\* P3, void\* P4)

Defines what items are to be shown on the status area.

## Parameters

-   **P1** - If 0, the syscall will set the *char* system variables at
    0x8804F48C and 0x8804F48D to 7, and the status flags to 0, while
    ignoring **flags**, **P3** and **P4**.\
    If 1, it will set the *char* system variables at 0x8804F48C and
    0x8804F48D to 7, and the status flags to 0x303, while ignoring
    **flags**, **P3** and **P4**.\
    If 2 and **P3** is not null, the *char* system variable at
    0x8804F48C is returned in **\*P3**; if 2 and **P4** is not null, the
    *char* system variable at 0x8804F48D is returned in **\*P4**.
    Whenever **P1** is 2, **flags** is ignored.\
    If 3 and **P3** is not null, the *char* system variable at
    0x8804F48C is set to **\*P3**; if 3 and **P4** is not null, the
    *char* system variable at 0x8804F48D is set to **\*P4**. Finally,
    and still assuming **P1** is 3, the status-icon-display-flags are
    set to **flags**.\
    If 4, the status flags are set to be the result of (current flags \|
    **flags**) (OR operation - this results in setting any previous
    flags plus the ones set in **flags**).\
    If 5, the status icon display flags are set to be the result of
    \~(current flags & **flags**) (NAND operation - this results in
    setting all the previous flags minus the ones set in **flags**).
-   **flags** - status area flags to operate with, when **P1** is 3, 4
    or 5. See the end of this page for a table of valid flags.
-   **P3** and **P4** are pointers to buffers that will be copied to
    system variables or will receive their contents, depending on each
    of the pointers being null or not, and on the values of **P1**.\

## Example

The following line of code will set the following items to appear on the
status bar: the battery meter, text (to be set with
[DefineStatusMessage]({{< ref "Syscalls/DefineStatusMessage.md" >}})),
custom glyphs (to be set with
[DefineStatusGlyph]({{< ref "Syscalls/DefineStatusGlyph.md" >}})) and
the keyboard input method (shown to the right of the battery icon when
any Alpha, Clip or Shift mode is enabled).

`DefineStatusAreaFlags(3, SAF_BATTERY | SAF_TEXT | SAF_GLYPH | SAF_ALPHA_SHIFT, 0, 0);`

## Appendix

Table of status bar flags:

| define                 | value  | meaning                                                                                          |
|------------------------|--------|--------------------------------------------------------------------------------------------------|
| SAF_BATTERY            | 0x0001 | Battery icon                                                                                     |
| SAF_ALPHA_SHIFT        | 0x0002 | Alpha/Shift/Clip keyboard modifier indicator                                                     |
| SAF_SETUP_INPUT_OUTPUT | 0x0004 | Input/Output mode (Math or Line)                                                                 |
| SAF_SETUP_FRAC_RESULT  | 0x0008 | Fraction display mode                                                                            |
| SAF_SETUP_ANGLE        | 0x0010 | Angle (trigonometry) mode                                                                        |
| SAF_SETUP_COMPLEX_MODE | 0x0020 | Complex number mode (Real, a+bi, r\<O)                                                           |
| SAF_SETUP_DISPLAY      | 0x0040 | Number display mode (Norm1, Norm2, Fix, Sci...)                                                  |
| SAF_TEXT               | 0x0100 | Custom text, to be set with [DefineStatusMessage]({{< ref "Syscalls/DefineStatusMessage.md" >}}) |
| SAF_GLYPH              | 0x0200 | Custom glyph, to be set with [DefineStatusGlyph]({{< ref "Syscalls/DefineStatusGlyph.md" >}})    |
|                        |        |                                                                                                  |
