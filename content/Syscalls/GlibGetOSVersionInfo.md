---
revisions:
- author: Dr-carlos
  comment: GlibGetOSVersionInfo is included in libfxcg
  timestamp: '2022-03-26T05:41:02Z'
title: GlibGetOSVersionInfo
---

## Synopsis

**Header:** fxcg/system.h\
**Syscall index:** 0x002B\
**Function signature:** int GlibGetOSVersionInfo(char \*a, char \*b,
short int \*c, short int \*d);

Gets the OS version broken up into 4 parts.\

## Parameters

-   **a** - Pointer to char to store the major version number to.
-   **b** - Pointer to char to store the minor version number to.
-   **c** - Unknown, 3rd part of the version?
-   **d** - Unknown, 4th part of the version?
