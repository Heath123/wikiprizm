---
revisions:
- author: Gbl08ma
  timestamp: '2014-07-29T19:04:43Z'
title: Bdisp_HeaderText
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x1D82\
**Function signature:** void Bdisp_HeaderText(void)

The function of this syscall is still unknown.\

## Comments

Yet to know what it does --[Gbl08ma](User:Gbl08ma)
([talk](User_talk:Gbl08ma)) 10:43, 9 August 2012 (EDT)
