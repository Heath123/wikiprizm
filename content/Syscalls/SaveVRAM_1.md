---
revisions:
- author: JosJuice
  timestamp: '2015-03-27T15:29:56Z'
title: SaveVRAM_1
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x1E62\
**Function signature:** *void* **SaveVRAM_1**(*void*)

Copies the whole [VRAM](VRAM) data to the address pointed to by
[GetSecondaryVramAddress]({{< ref "Syscalls/GetSecondaryVramAddress.md" >}}).
You can load this data back using
[LoadVRAM_1]({{< ref "Syscalls/LoadVRAM_1.md" >}}).\

## Comments

This function is called by the OS when entering the Main Menu. Any
contents on the VRAM-saving buffer will be overwritten when this
function is called.
