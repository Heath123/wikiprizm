---
revisions:
- author: ProgrammerNerd
  comment: "Created page with \u201C{{Syscall \\| name = Bdisp_SetDDRegisterB\n\\\
    | index = 0x0194 \\| signature = void Bdisp_SetDDRegisterB(int\ncolorMode); \\\
    | header = fxcg/display.h {{NoLibfxcg}} \\| synopsis =\nUsed to e\u2026\u201D"
  timestamp: '2015-02-23T00:05:40Z'
title: Bdisp_SetDDRegisterB
---

## Synopsis

**Header:** fxcg/display.h *(Not yet in
[libfxcg](https://github.com/Jonimoose/libfxcg))*\
**Syscall index:** 0x0194\
**Function signature:** void Bdisp_SetDDRegisterB(int colorMode);

Used to enable/disable 8-color mode. Unlike
[Bdisp_EnableColor]({{< ref "Syscalls/Bdisp_EnableColor.md" >}}) the
results of this function are immediate.\

## Parameters

**colorMode**

-   If nonzero disable 8-color mode.
-   If zero enable 8-color mode.\

## Comments

Here is a disassembly of the function:

    Bdisp_SetDDRegisterB:
            sts.l   pr, @-r15
            mov.l   #colorMode, r6
            extu.b  r4, r0
            tst r0, r0
            bt/s    r0_zero
            mov.b   r4, @r6
            cmp/eq  #1, r0
            bt  r0_one
            bra exit
            nop
    ! ---------------------------------------------------------------------------

    r0_zero:
            bsr Bdisp_DDRegisterSelect
            mov #11, r4     ! Select register 11
            mov #0xFFFFFFB4, r2
            mov #1, r6
            shll8   r2
            mov #0, r4
            shll16  r2      ! r2 = 0xB4000000
            mov.w   r6, @r2     ! Enable 8 color mode
            synco
            bra sub_8004CB96
            lds.l   @r15+, pr
    ! ---------------------------------------------------------------------------

    r0_one:
            bsr Bdisp_DDRegisterSelect
            mov #11, r4     ! Select register 11
            mov #0xFFFFFFB4, r2
            mov #0, r6
            shll8   r2
            mov #1, r4
            shll16  r2      ! r2 = 0xB4000000
            mov.w   r6, @r2     ! Disable 8 color mode
            synco
            bra sub_8004CB96
            lds.l   @r15+, pr
    ! ---------------------------------------------------------------------------

    exit:
            lds.l   @r15+, pr
            rts
            nop
    ! End of function Bdisp_SetDDRegisterB
