---
revisions:
- author: Gbl08ma
  timestamp: '2016-05-10T19:25:06Z'
title: PrintMini
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x023C\
**Function signature:** void PrintMini( int\* x, int\* y, const char\*
string, int mode_flags, unsigned int xlimit, int P6, int P7, int color,
int back_color, int writeflag, int P11)

Draws a line of text on the VRAM using the [18 px
font]({{< ref "Fonts.md" >}}), as seen on the status bar of the USB
connection screen, or inside a eActivity line note, for instance.\

## Parameters

-   *int\** **x** - Pointer to an integer containing the position of the
    text on a horizontal axis, in pixels. It is automatically updated to
    the position for the next character as the text is drawn
-   *int\** **y** - Pointer to an integer containing the position of the
    text on a vertical axis, in pixels. Automatically incremented by 24
    pixels (status area) unless **mode_flags** is set to 0x40.
-   *unsigned char\** **string** - The string to display. Can include
    [multi-byte characters]({{< ref "Multi-byte_strings.md" >}}).
-   *int* **mode_flags** - 0x40 allows for drawing text on the status
    area; 0x04 inverts **color** and **back_color**; 0x02 does not use
    **back_color**
-   *unsigned int* **xlimit** - Upper horizontal limit - text will be
    cut past this point. Set to 0xFFFFFFFF for it to be ignored.
-   *int* **P6** - Still unknown, set to 0
-   *int* **P7** - Still unknown, set to 0
-   *int* **color** - The color of the characters
-   *int* **back_color** - The color of the characters' background.
    Depends on **mode_flags**
-   *int* **writeflag** - Set to 1 for the text to be actually drawn;
    setting to 0 only makes the value of **x** update as if the text was
    being drawn.
-   *int* **P11** - Still unknown, set to 0
