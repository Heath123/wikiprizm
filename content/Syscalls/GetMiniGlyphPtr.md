---
revisions:
- author: Gbl08ma
  timestamp: '2014-08-04T12:06:21Z'
title: GetMiniGlyphPtr
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x01E9\
**Function signature:** void\* GetMiniGlyphPtr(unsigned short
mb_glyph_no, unsigned short\* glyph_info)

Gets a pointer to a specified mini-size glyph.\

## Parameters

-   **mb_glyph_no** - [multi-byte]({{< ref "Multi-byte_strings.md" >}})
    number of the glyph. Most ASCII codes are supported just fine.
-   **glyph_info** - pointer to a short that will hold the glyph width,
    once the function returns.\

## Returns

A pointer to the specified mini-size glyph.
