---
revisions:
- author: Gbl08ma
  timestamp: '2014-08-01T14:30:36Z'
title: Comm_TryCheckPacket
---

## Synopsis

**Header:** fxcg/serial.h\
**Syscall index:** 0x1396\
**Function signature:** int Comm_TryCheckPacket(unsigned char subtype)

Sends a *Protocol 7.00* check packet of a certain subtype. Check packets
are sent to verify if the other device is connected.\

## Parameters

-   **subtype** - subtype of the packet to send: 0 is sent at start of
    communication, 1 is sent during the communication.\

## Returns

The meaning of the return value is unknown, but is certainly related to
the result of the check.\

## Comments

For more information on *Protocol 7.00*, see
[fxReverse.pdf](http://tny.im/dl/fxReverse2x.pdf).
