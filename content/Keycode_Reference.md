---
revisions:
- author: tswilliamson
  comment: /\* Program Key Codes \*/
  timestamp: '2017-04-01T03:09:56Z'
title: Keycode_Reference
---

The Prizm's syscalls and related [keyboard]({{< ref "keyboard.md" >}})
functions use different methods of representing keys. These values vary
in size and are tied to specific groups of syscalls. The
[Reading_Input]({{< ref "Tutorials/Reading_Input.md" >}}) tutorial page
has information on such syscalls and their uses.

## Matrix Key Codes {#matrix_key_codes}

The matrix key codes are a 2 byte column/row pair specified as 0xCCRR.
These correlate to the column and row of the keyboard matrix that the
key is located at. The following functions use this key format:

-   [GetKeyWait_OS]({{< ref "Syscalls/Keyboard/GetKeyWait_OS.md" >}})
-   [Keyboard_PutKeycode]({{< ref "Syscalls/Keyboard/Keyboard_PutKeycode.md" >}})
    (first two parameters)

The two tables in this section show the same information, presented in
different ways.

<div>
<table style="margin-bottom:-1px; border-bottom:0; text-align: center;" class="wikitable">
<caption>
</caption>
<tbody><tr>
<th colspan="6">Matrix Hexadecimal Codes
</th></tr>
<tr>
<td style="width:16.6666666666667%">F1<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">070A</span>
</td>
<td style="width:16.6666666666667%">F2<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">060A</span>
</td>
<td style="width:16.6666666666667%">F3<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">050A</span>
</td>
<td style="width:16.6666666666667%">F4<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">040A</span>
</td>
<td style="width:16.6666666666667%">F5<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">030A</span>
</td>
<td style="width:16.6666666666667%">F6<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">020A</span>
</td></tr>
<tr>
<td><span style="color:#ccac00;">Shift</span><br /><span style="font-size:88%;line-height:1.0em;color:#555555;">0709</span>
</td>
<td>Optn<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">0609</span>
</td>
<td>Vars<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">0509</span>
</td>
<td>Menu<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">0409</span>
</td>
<td colspan="2" rowspan="2">
<table style="margin-left:auto; margin-right:auto; line-height: 1em;">

<tbody><tr>
<td colspan="3"><span style="font-size:88%;line-height:1.0em;color:#555555;">0209</span><br />▴
</td></tr>
<tr>
<td width="33%;">◂<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">0309</span>
</td>
<td width="33%;">&#160;
</td>
<td width="33%;">▸<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">0208</span>
</td></tr>
<tr>
<td colspan="3">▾<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">0308</span>
</td></tr></tbody></table>
</td></tr>
<tr>
<td><span style="color:#ff0000;">Alpha</span><br /><span style="font-size:88%;line-height:1.0em;color:#555555;">0708</span>
</td>
<td>&#x1d4cd;²<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">0608</span>
</td>
<td>^<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">0508</span>
</td>
<td>Exit<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">0408</span>
</td></tr>
<tr>
<td>X,θ,T<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">0707</span>
</td>
<td>log<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">0607</span>
</td>
<td>ln<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">0507</span>
</td>
<td>sin<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">0407</span>
</td>
<td>cos<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">0307</span>
</td>
<td>tan<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">0207</span>
</td></tr>
<tr>
<td>a<sup>b</sup>/<sub>c</sub><br /><span style="font-size:88%;line-height:1.0em;color:#555555;">0706</span>
</td>
<td>F↔D<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">0606</span>
</td>
<td>(<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">0506</span>
</td>
<td>)<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">0406</span>
</td>
<td>,<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">0306</span>
</td>
<td>→<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">0206</span>
</td></tr></tbody></table>
<table style="margin-top:0px; text-align:center;" class="wikitable">

<tbody><tr>
<td style="width:20%">7<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">0705</span>
</td>
<td style="width:20%">8<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">0605</span>
</td>
<td style="width:20%">9<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">0505</span>
</td>
<td style="width:20%">DEL<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">0405</span>
</td>
<td style="width:20%">AC<sup>/ON</sup><br /><span style="font-size:88%;line-height:1.0em;color:#555555;">0101</span>
</td></tr>
<tr>
<td>4<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">0704</span>
</td>
<td>5<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">0604</span>
</td>
<td>6<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">0504</span>
</td>
<td>×<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">0404</span>
</td>
<td>÷<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">0304</span>
</td></tr>
<tr>
<td>1<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">0703</span>
</td>
<td>2<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">0603</span>
</td>
<td>3<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">0503</span>
</td>
<td>+<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">0403</span>
</td>
<td>-<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">0303</span>
</td></tr>
<tr>
<td>0<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">0702</span>
</td>
<td>.<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">0602</span>
</td>
<td>EXP<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">0502</span>
</td>
<td>(‒)<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">0402</span>
</td>
<td><span style="color:#0000ff;">EXE</span><br /><span style="font-size:88%;line-height:1.0em;color:#555555;">0302</span>
</td></tr></tbody></table>
</div>

<table class="wikitable">
<caption>
</caption>
<tbody><tr>
<th colspan="8">Matrix Hexadecimal Codes
</th></tr>
<tr>
<td>F1
</td>
<td>F2
</td>
<td>F3
</td>
<td>F4
</td>
<td>F5
</td>
<td>F6
</td>
<td>
</td>
<th>0A
</th></tr>
<tr>
<td>SHIFT
</td>
<td>OPTN
</td>
<td>VARS
</td>
<td>MENU
</td>
<td>←
</td>
<td>↑
</td>
<td>
</td>
<th>09
</th></tr>
<tr>
<td>ALPHA
</td>
<td>x^2
</td>
<td>^
</td>
<td>EXIT
</td>
<td>↓
</td>
<td>→
</td>
<td>
</td>
<th>08
</th></tr>
<tr>
<td>X,θ,T
</td>
<td>log
</td>
<td>ln
</td>
<td>sin
</td>
<td>cos
</td>
<td>tan
</td>
<td>
</td>
<th>07
</th></tr>
<tr>
<td>a b/c
</td>
<td>F↔D
</td>
<td>(
</td>
<td>)
</td>
<td>,
</td>
<td>→
</td>
<td>
</td>
<th>06
</th></tr>
<tr>
<td>7
</td>
<td>8
</td>
<td>9
</td>
<td>DEL
</td>
<td>
</td>
<td>
</td>
<td>
</td>
<th>05
</th></tr>
<tr>
<td>4
</td>
<td>5
</td>
<td>6
</td>
<td>×
</td>
<td>÷
</td>
<td>
</td>
<td>
</td>
<th>04
</th></tr>
<tr>
<td>1
</td>
<td>2
</td>
<td>3
</td>
<td>+
</td>
<td>-
</td>
<td>
</td>
<td>
</td>
<th>03
</th></tr>
<tr>
<td>0
</td>
<td>.
</td>
<td>EXP
</td>
<td>(-)
</td>
<td>EXE
</td>
<td>
</td>
<td>
</td>
<th>02
</th></tr>
<tr>
<td>
</td>
<td>
</td>
<td>
</td>
<td>
</td>
<td>
</td>
<td>
</td>
<td>AC/On
</td>
<th>01
</th></tr>
<tr>
<th>07
</th>
<th>06
</th>
<th>05
</th>
<th>04
</th>
<th>03
</th>
<th>02
</th>
<th>01
</th>
<td>
</td></tr></tbody></table>

## Program Key Codes {#program_key_codes}

The key codes here are decimal values made up of the column and row as
CR. The following functions use this key format:

-   [PRGM_GetKey]({{< ref "Syscalls/Keyboard/PRGM_GetKey_OS.md" >}}) -
    Function to emulate legacy functionality, see
    [PRGM_GetKey_OS]({{< ref "Syscalls/Keyboard/PRGM_GetKey_OS.md" >}})
    for definition.

<table style="margin-bottom:-1px; border-bottom:0; text-align: center;" class="wikitable">
<caption>
</caption>
<tbody><tr>
<th colspan="6">Program Codes
</th></tr>
<tr>
<td style="width:16.6666666666667%">F1<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">79</span>
</td>
<td style="width:16.6666666666667%">F2<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">69</span>
</td>
<td style="width:16.6666666666667%">F3<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">59</span>
</td>
<td style="width:16.6666666666667%">F4<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">49</span>
</td>
<td style="width:16.6666666666667%">F5<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">39</span>
</td>
<td style="width:16.6666666666667%">F6<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">29</span>
</td></tr>
<tr>
<td><span style="color:#ccac00;">Shift</span><br /><span style="font-size:88%;line-height:1.0em;color:#555555;">78</span>
</td>
<td>Optn<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">68</span>
</td>
<td>Vars<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">58</span>
</td>
<td>Menu<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">48</span>
</td>
<td colspan="2" rowspan="2">
<table style="margin-left:auto; margin-right:auto; line-height: 1em;">

<tbody><tr>
<td colspan="3"><span style="font-size:88%;line-height:1.0em;color:#555555;">28</span><br />▴
</td></tr>
<tr>
<td width="33%;">◂<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">38</span>
</td>
<td width="33%;">&#160;
</td>
<td width="33%;">▸<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">27</span>
</td></tr>
<tr>
<td colspan="3">▾<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">37</span>
</td></tr></tbody></table>
</td></tr>
<tr>
<td><span style="color:#ff0000;">Alpha</span><br /><span style="font-size:88%;line-height:1.0em;color:#555555;">77</span>
</td>
<td>&#x1d4cd;²<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">67</span>
</td>
<td>^<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">57</span>
</td>
<td>Exit<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">47</span>
</td></tr>
<tr>
<td>X,θ,T<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">76</span>
</td>
<td>log<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">66</span>
</td>
<td>ln<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">56</span>
</td>
<td>sin<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">46</span>
</td>
<td>cos<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">36</span>
</td>
<td>tan<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">26</span>
</td></tr>
<tr>
<td>a<sup>b</sup>/<sub>c</sub><br /><span style="font-size:88%;line-height:1.0em;color:#555555;">75</span>
</td>
<td>F↔D<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">65</span>
</td>
<td>(<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">55</span>
</td>
<td>)<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">45</span>
</td>
<td>,<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">35</span>
</td>
<td>→<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">25</span>
</td></tr></tbody></table>
<table style="margin-top:0px; text-align:center;" class="wikitable">

<tbody><tr>
<td style="width:20%">7<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">74</span>
</td>
<td style="width:20%">8<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">64</span>
</td>
<td style="width:20%">9<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">54</span>
</td>
<td style="width:20%">DEL<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">44</span>
</td>
<td style="width:20%">AC<sup>/ON</sup><br /><span style="font-size:88%;line-height:1.0em;color:#555555;">10</span>
</td></tr>
<tr>
<td>4<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">73</span>
</td>
<td>5<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">63</span>
</td>
<td>6<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">53</span>
</td>
<td>×<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">43</span>
</td>
<td>÷<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">33</span>
</td></tr>
<tr>
<td>1<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">72</span>
</td>
<td>2<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">62</span>
</td>
<td>3<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">52</span>
</td>
<td>+<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">42</span>
</td>
<td>-<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">32</span>
</td></tr>
<tr>
<td>0<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">71</span>
</td>
<td>.<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">61</span>
</td>
<td>EXP<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">51</span>
</td>
<td>(‒)<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">41</span>
</td>
<td><span style="color:#0000ff;">EXE</span><br /><span style="font-size:88%;line-height:1.0em;color:#555555;">31</span>
</td></tr></tbody></table>

## Character and Control Codes {#character_and_control_codes}

Character codes are not directly mapped to the physical keys on the
keyboard. The values use the state of key modifiers to return a value
pertaining to the specific function of a key (pressing EXIT returns a
code that's different from when pressing SHIFT then EXIT). The following
syscalls use these codes:

-   [GetKey]({{< ref "Syscalls/Keyboard/GetKey.md" >}})
-   [Keyboard_PutKeycode]({{< ref "Syscalls/Keyboard/Keyboard_PutKeycode.md" >}})
    (third parameter)
-   [EditMBStringCtrl]({{< ref "Syscalls/Text_and_expression_editing/EditMBStringCtrl.md" >}})
    and variants
-   [EditMBStringChar]({{< ref "Syscalls/Text_and_expression_editing/EditMBStringChar.md" >}})
    and variants

The values in the tables below are in hexadecimal.

<div style="padding-right: 5px;">
<table style="margin-bottom:-1px; border-bottom:0; text-align: center;" class="wikitable">
<caption>
</caption>
<tbody><tr>
<th colspan="6">C&amp;C Codes (no modifier)
</th></tr>
<tr>
<td style="width:16.6666666666667%">F1<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">7539</span>
</td>
<td style="width:16.6666666666667%">F2<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">753A</span>
</td>
<td style="width:16.6666666666667%">F3<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">753B</span>
</td>
<td style="width:16.6666666666667%">F4<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">753C</span>
</td>
<td style="width:16.6666666666667%">F5<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">753D</span>
</td>
<td style="width:16.6666666666667%">F6<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">753E</span>
</td></tr>
<tr>
<td><span style="color:#ccac00;">Shift</span><br /><span style="font-size:88%;line-height:1.0em;color:#555555;">7536</span>
</td>
<td>Optn<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">7538</span>
</td>
<td>Vars<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">7540</span>
</td>
<td>Menu<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">7533</span>
</td>
<td colspan="2" rowspan="2">
<table style="margin-left:auto; margin-right:auto; line-height: 1em;">

<tbody><tr>
<td colspan="3"><span style="font-size:88%;line-height:1.0em;color:#555555;">7542</span><br />▴
</td></tr>
<tr>
<td width="33%;">◂<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">7544</span>
</td>
<td width="33%;">&#160;
</td>
<td width="33%;">▸<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">7545</span>
</td></tr>
<tr>
<td colspan="3">▾<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">7547</span>
</td></tr></tbody></table>
</td></tr>
<tr>
<td><span style="color:#ff0000;">Alpha</span><br /><span style="font-size:88%;line-height:1.0em;color:#555555;">7537</span>
</td>
<td>&#x1d4cd;²<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">008B</span>
</td>
<td>^<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">00A8</span>
</td>
<td>Exit<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">7532</span>
</td></tr>
<tr>
<td>X,θ,T<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">7531</span>
</td>
<td>log<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">0095</span>
</td>
<td>ln<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">0085</span>
</td>
<td>sin<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">0081</span>
</td>
<td>cos<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">0082</span>
</td>
<td>tan<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">0083</span>
</td></tr>
<tr>
<td>a<sup>b</sup>/<sub>c</sub><br /><span style="font-size:88%;line-height:1.0em;color:#555555;">00BB</span>
</td>
<td>F↔D<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">755E</span>
</td>
<td>(<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">0028</span>
</td>
<td>)<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">0029</span>
</td>
<td>,<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">002C</span>
</td>
<td>→<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">000E</span>
</td></tr></tbody></table>
<table style="margin-top:0px; text-align:center;" class="wikitable">

<tbody><tr>
<td style="width:20%">7<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">0037</span>
</td>
<td style="width:20%">8<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">0038</span>
</td>
<td style="width:20%">9<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">0039</span>
</td>
<td style="width:20%">DEL<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">7549</span>
</td>
<td style="width:20%">AC<sup>/ON</sup><br /><span style="font-size:88%;line-height:1.0em;color:#555555;">753F</span>
</td></tr>
<tr>
<td>4<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">0034</span>
</td>
<td>5<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">0035</span>
</td>
<td>6<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">0036</span>
</td>
<td>×<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">00A9</span>
</td>
<td>÷<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">00B9</span>
</td></tr>
<tr>
<td>1<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">0031</span>
</td>
<td>2<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">0032</span>
</td>
<td>3<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">0033</span>
</td>
<td>+<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">0089</span>
</td>
<td>-<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">0099</span>
</td></tr>
<tr>
<td>0<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">0030</span>
</td>
<td>.<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">002E</span>
</td>
<td>EXP<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">000F</span>
</td>
<td>(‒)<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">0087</span>
</td>
<td><span style="color:#0000ff;">EXE</span><br /><span style="font-size:88%;line-height:1.0em;color:#555555;">7534</span>
</td></tr></tbody></table>
</div>
<div style="padding-right: 5px;">
<table style="margin-bottom:-1px; border-bottom:0; text-align: center;" class="wikitable">
<caption>
</caption>
<tbody><tr>
<th colspan="6">C&amp;C Codes (Shift)
</th></tr>
<tr>
<td style="width:16.6666666666667%">F1<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">7539</span>
</td>
<td style="width:16.6666666666667%">F2<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">753A</span>
</td>
<td style="width:16.6666666666667%">F3<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">753B</span>
</td>
<td style="width:16.6666666666667%">F4<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">753C</span>
</td>
<td style="width:16.6666666666667%">F5<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">753D</span>
</td>
<td style="width:16.6666666666667%">F6<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">753E</span>
</td></tr>
<tr>
<td><span style="color:#ccac00;">Shift</span><br /><span style="font-size:88%;line-height:1.0em;color:#555555;">7536</span>
</td>
<td>Optn<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>756B</b></span>
</td>
<td>Vars<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>754C</b></span>
</td>
<td>Menu<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>7555</b></span>
</td>
<td colspan="2" rowspan="2">
<table style="margin-left:auto; margin-right:auto; line-height: 1em;">

<tbody><tr>
<td colspan="3"><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>7564</b></span><br />▴
</td></tr>
<tr>
<td width="33%;">◂<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>756E</b></span>
</td>
<td width="33%;">&#160;
</td>
<td width="33%;">▸<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>756F</b></span>
</td></tr>
<tr>
<td colspan="3">▾<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>7565</b></span>
</td></tr></tbody></table>
</td></tr>
<tr>
<td><span style="color:#ff0000;">Alpha</span><br /><span style="font-size:88%;line-height:1.0em;color:#555555;">7537</span>
</td>
<td>&#x1d4cd;²<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>0086</b></span>
</td>
<td>^<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>00B8</b></span>
</td>
<td>Exit<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>754D</b></span>
</td></tr>
<tr>
<td>X,θ,T<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>7F54</b></span>
</td>
<td>log<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>00B5</b></span>
</td>
<td>ln<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>00A5</b></span>
</td>
<td>sin<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>0091</b></span>
</td>
<td>cos<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>0092</b></span>
</td>
<td>tan<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>0093</b></span>
</td></tr>
<tr>
<td>a<sup>b</sup>/<sub>c</sub><br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>7566</b></span>
</td>
<td>F↔D<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>754A</b></span>
</td>
<td>(<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>0096</b></span>
</td>
<td>)<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>009B</b></span>
</td>
<td>,<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>--</b></span>
</td>
<td>→<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>--</b></span>
</td></tr></tbody></table>
<table style="margin-top:0px; text-align:center;" class="wikitable">

<tbody><tr>
<td style="width:20%">7<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>7567</b></span>
</td>
<td style="width:20%">8<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>7562</b></span>
</td>
<td style="width:20%">9<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>7554</b></span>
</td>
<td style="width:20%">DEL<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>7551</b></span>
</td>
<td style="width:20%">AC<sup>/ON</sup><br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>--</b></span>
</td></tr>
<tr>
<td>4<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>7594</b></span>
</td>
<td>5<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>7595</b></span>
</td>
<td>6<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>--</b></span>
</td>
<td>×<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>007B</b></span>
</td>
<td>÷<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>007D</b></span>
</td></tr>
<tr>
<td>1<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>7F51</b></span>
</td>
<td>2<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>7F40</b></span>
</td>
<td>3<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>--</b></span>
</td>
<td>+<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>005B</b></span>
</td>
<td>-<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>005D</b></span>
</td></tr>
<tr>
<td>0<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>7F50</b></span>
</td>
<td>.<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>003D</b></span>
</td>
<td>EXP<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>00D0</b></span>
</td>
<td>(‒)<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>00C0</b></span>
</td>
<td><span style="color:#0000ff;">EXE</span><br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>000D</b></span>
</td></tr></tbody></table>
</div>
<div style="padding-right: 5px;">
<table style="margin-bottom:-1px; border-bottom:0; text-align: center;" class="wikitable">
<caption>
</caption>
<tbody><tr>
<th colspan="6">C&amp;C Codes (Alpha uppercase)
</th></tr>
<tr>
<td style="width:16.6666666666667%">F1<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">7539</span>
</td>
<td style="width:16.6666666666667%">F2<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">753A</span>
</td>
<td style="width:16.6666666666667%">F3<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">753B</span>
</td>
<td style="width:16.6666666666667%">F4<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">753C</span>
</td>
<td style="width:16.6666666666667%">F5<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">753D</span>
</td>
<td style="width:16.6666666666667%">F6<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">753E</span>
</td></tr>
<tr>
<td><span style="color:#ccac00;">Shift</span><br /><span style="font-size:88%;line-height:1.0em;color:#555555;">7536</span>
</td>
<td>Optn<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">7538</span>
</td>
<td>Vars<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">7540</span>
</td>
<td>Menu<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">7533</span>
</td>
<td colspan="2" rowspan="2">
<table style="margin-left:auto; margin-right:auto; line-height: 1em;">

<tbody><tr>
<td colspan="3"><span style="font-size:88%;line-height:1.0em;color:#555555;">7542</span><br />▴
</td></tr>
<tr>
<td width="33%;">◂<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">7544</span>
</td>
<td width="33%;">&#160;
</td>
<td width="33%;">▸<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">7545</span>
</td></tr>
<tr>
<td colspan="3">▾<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">7547</span>
</td></tr></tbody></table>
</td></tr>
<tr>
<td><span style="color:#ff0000;">Alpha</span><br /><span style="font-size:88%;line-height:1.0em;color:#555555;">7537</span>
</td>
<td>&#x1d4cd;²<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>00CD</b></span>
</td>
<td>^<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>00CE</b></span>
</td>
<td>Exit<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">7532</span>
</td></tr>
<tr>
<td>X,θ,T<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>0041</b></span>
</td>
<td>log<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>0042</b></span>
</td>
<td>ln<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>0043</b></span>
</td>
<td>sin<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>0044</b></span>
</td>
<td>cos<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>0045</b></span>
</td>
<td>tan<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>0046</b></span>
</td></tr>
<tr>
<td>a<sup>b</sup>/<sub>c</sub><br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>0047</b></span>
</td>
<td>F↔D<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>0048</b></span>
</td>
<td>(<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>0049</b></span>
</td>
<td>)<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>004A</b></span>
</td>
<td>,<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>004B</b></span>
</td>
<td>→<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>004C</b></span>
</td></tr></tbody></table>
<table style="margin-top:0px; text-align:center;" class="wikitable">

<tbody><tr>
<td style="width:20%">7<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>004D</b></span>
</td>
<td style="width:20%">8<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>004E</b></span>
</td>
<td style="width:20%">9<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>004F</b></span>
</td>
<td style="width:20%">DEL<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>755D</b></span>
</td>
<td style="width:20%">AC<sup>/ON</sup><br /><span style="font-size:88%;line-height:1.0em;color:#555555;">753F</span>
</td></tr>
<tr>
<td>4<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>0050</b></span>
</td>
<td>5<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>0051</b></span>
</td>
<td>6<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>0052</b></span>
</td>
<td>×<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>0053</b></span>
</td>
<td>÷<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>0054</b></span>
</td></tr>
<tr>
<td>1<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>0055</b></span>
</td>
<td>2<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>0056</b></span>
</td>
<td>3<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>0057</b></span>
</td>
<td>+<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>0058</b></span>
</td>
<td>-<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>0059</b></span>
</td></tr>
<tr>
<td>0<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>005A</b></span>
</td>
<td>.<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>0020</b></span>
</td>
<td>EXP<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>0022</b></span>
</td>
<td>(‒)<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>7576</b></span>
</td>
<td><span style="color:#0000ff;">EXE</span><br /><span style="font-size:88%;line-height:1.0em;color:#555555;">7534</span>
</td></tr></tbody></table>
</div>
<div style="padding-right: 5px;">
<table style="margin-bottom:-1px; border-bottom:0; text-align: center;" class="wikitable">
<caption>
</caption>
<tbody><tr>
<th colspan="6">C&amp;C Codes (Alpha lowercase)
</th></tr>
<tr>
<td style="width:16.6666666666667%">F1<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">7539</span>
</td>
<td style="width:16.6666666666667%">F2<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">753A</span>
</td>
<td style="width:16.6666666666667%">F3<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">753B</span>
</td>
<td style="width:16.6666666666667%">F4<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">753C</span>
</td>
<td style="width:16.6666666666667%">F5<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">753D</span>
</td>
<td style="width:16.6666666666667%">F6<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">753E</span>
</td></tr>
<tr>
<td><span style="color:#ccac00;">Shift</span><br /><span style="font-size:88%;line-height:1.0em;color:#555555;">7536</span>
</td>
<td>Optn<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">7538</span>
</td>
<td>Vars<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">7540</span>
</td>
<td>Menu<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">7533</span>
</td>
<td colspan="2" rowspan="2">
<table style="margin-left:auto; margin-right:auto; line-height: 1em;">

<tbody><tr>
<td colspan="3"><span style="font-size:88%;line-height:1.0em;color:#555555;">7542</span><br />▴
</td></tr>
<tr>
<td width="33%;">◂<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">7544</span>
</td>
<td width="33%;">&#160;
</td>
<td width="33%;">▸<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">7545</span>
</td></tr>
<tr>
<td colspan="3">▾<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">7547</span>
</td></tr></tbody></table>
</td></tr>
<tr>
<td><span style="color:#ff0000;">Alpha</span><br /><span style="font-size:88%;line-height:1.0em;color:#555555;">7537</span>
</td>
<td>&#x1d4cd;²<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>00CD</b></span>
</td>
<td>^<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>00CE</b></span>
</td>
<td>Exit<br /><span style="font-size:88%;line-height:1.0em;color:#555555;">7532</span>
</td></tr>
<tr>
<td>X,θ,T<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>0061</b></span>
</td>
<td>log<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>0062</b></span>
</td>
<td>ln<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>0063</b></span>
</td>
<td>sin<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>0064</b></span>
</td>
<td>cos<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>0065</b></span>
</td>
<td>tan<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>0066</b></span>
</td></tr>
<tr>
<td>a<sup>b</sup>/<sub>c</sub><br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>0067</b></span>
</td>
<td>F↔D<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>0068</b></span>
</td>
<td>(<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>0069</b></span>
</td>
<td>)<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>006A</b></span>
</td>
<td>,<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>006B</b></span>
</td>
<td>→<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>006C</b></span>
</td></tr></tbody></table>
<table style="margin-top:0px; text-align:center;" class="wikitable">

<tbody><tr>
<td style="width:20%">7<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>006D</b></span>
</td>
<td style="width:20%">8<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>006E</b></span>
</td>
<td style="width:20%">9<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>006F</b></span>
</td>
<td style="width:20%">DEL<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>755D</b></span>
</td>
<td style="width:20%">AC<sup>/ON</sup><br /><span style="font-size:88%;line-height:1.0em;color:#555555;">753F</span>
</td></tr>
<tr>
<td>4<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>0070</b></span>
</td>
<td>5<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>0071</b></span>
</td>
<td>6<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>0072</b></span>
</td>
<td>×<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>0073</b></span>
</td>
<td>÷<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>0074</b></span>
</td></tr>
<tr>
<td>1<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>0075</b></span>
</td>
<td>2<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>0076</b></span>
</td>
<td>3<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>0077</b></span>
</td>
<td>+<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>0078</b></span>
</td>
<td>-<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>0079</b></span>
</td></tr>
<tr>
<td>0<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>007A</b></span>
</td>
<td>.<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>0020</b></span>
</td>
<td>EXP<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>0022</b></span>
</td>
<td>(‒)<br /><span style="font-size:88%;line-height:1.0em;color:#555555;"><b>7576</b></span>
</td>
<td><span style="color:#0000ff;">EXE</span><br /><span style="font-size:88%;line-height:1.0em;color:#555555;">7534</span>
</td></tr></tbody></table>
</div>
<div style="clear:left;"></div>

### Notes

-   The values of the characters in Alpha mode are the same as their
    ASCII codes, both for upper- and lower-case.
-   Certain keys have a different value when modifiers are on, even when
    there is nothing printed on the keyboard to indicate such. Examples
    include Shift+Optn and Shift+cursor keys.
-   Control codes start at 0x7530 (decimal: 30000) and are meant to be
    redirected to syscalls such as
    [EditMBStringCtrl]({{< ref "Syscalls/Text_and_expression_editing/EditMBStringCtrl.md" >}});
    character codes are much lower values and should be redirected to
    syscalls like
    [EditMBStringChar]({{< ref "Syscalls/Text_and_expression_editing/EditMBStringChar.md" >}}).

## Direct Keyboard Matrix {#direct_keyboard_matrix}

*TODO*
