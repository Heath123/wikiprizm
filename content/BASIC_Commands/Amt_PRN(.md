---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= Amt_PRN( = == Description == This\ncommand returns\
    \ the principal and interest paid for payment PM1. ==\nSyntax\n==\u2019\u2018\u2019\
    Amt_PRN(\u2019\u2019\u2019\u2018\u2019PM1\u2019\u2018,\u2019\u2018PM2\u2019\u2018\
    ,\u2019\u2018I%\u2019\u2018,\u2019\u2018PV\u2019\u2018,\u2019\u2018PMT\u2019\u2018\
    ,\u2019\u2018P/Y\u2019\u2018,\u2019\u2018C/\u2026\u2019"
  timestamp: '2012-02-15T23:36:49Z'
title: Amt_PRN(
---

# Amt_PRN(

## Description

This command returns the principal and interest paid for payment PM1.

## Syntax

**Amt_PRN(***PM1*,*PM2*,*I%*,*PV*,*PMT*,*P/Y*,*C/Y***)**

## Example

`Amt_PRN(12,10,5.5,1000,1000,12,12)`
