---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= \xB0(deg) = == Description == This command\n\
    specifies the evaluation in degree mode. == Syntax ==\u2019\u2018Expression\u2019\
    \u2019\n\u2019\u2019\u2018\xB0\u2019\u2019\u2019 == Example == sin 30\xB0 \\[\\\
    [Category:BASIC_Commands\\]\\]\u2019"
  timestamp: '2012-02-24T20:34:36Z'
title: "\xB0(deg)"
---

# °(deg)

## Description

This command specifies the evaluation in degree mode.

## Syntax

*Expression* **°**

## Example

`sin 30°`
