---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= InvPoissonCD( = == Description == This\ncommand\
    \ returns the cumulative inverse Poisson distribution. ==\nSyntax ==\u2019\u2018\
    \u2019InvPoissonCD(\u2019\u2019\u2019\u2018\u2019p\u2019\u2018,\u2019\u2018lamda\u2019\
    \u2019\u2019\u2019\u2018)\u2019\u2019\u2019 == Example == \u2026\u2019"
  timestamp: '2012-02-16T00:50:47Z'
title: InvPoissonCD(
---

# InvPoissonCD(

## Description

This command returns the cumulative inverse Poisson distribution.

## Syntax

**InvPoissonCD(***p*,*lamda***)**

## Example

`InvPoissonCD(.35,5)`
