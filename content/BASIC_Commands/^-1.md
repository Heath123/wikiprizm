---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= ^-1 = == Description == This command\nreturns\
    \ the value^-1(aka reciprocal of the value). == Syntax\n==\u2019\u2018Value\u2019\
    \u2019\u2019\u2019\u2018^-1\u2019\u2019\u2019 == Example == 3.5^-1\n\\[\\[Category:BASIC_Commands\\\
    ]\\]\u2019"
  timestamp: '2012-02-24T20:16:55Z'
title: ^-1
---

# ^-1

## Description

This command returns the value^-1(aka reciprocal of the value).

## Syntax

*Value***^-1**

## Example

`3.5^-1`
