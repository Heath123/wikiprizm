---
revisions:
- author: Turiqwalrus
  comment: "Created page with \u2018= Cosh\xAF\xB9 = == Description == This command\n\
    returns the hyperbolic arccosine of the value. == Syntax\n==\u2019\u2018\u2019\
    Cosh\xAF\xB9\u2019\u2019\u2019 \u2018\u2019Value\u2019\u2019 == Example == Cosh\xAF\
    \xB9 .75\u2019"
  timestamp: '2012-02-29T20:28:18Z'
title: "cosh\xAF\xB9"
---

# Cosh¯¹

## Description

This command returns the hyperbolic arccosine of the value.

## Syntax

**Cosh¯¹** *Value*

## Example

`Cosh¯¹ .75`
