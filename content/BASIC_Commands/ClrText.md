---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= ClrText = == Description == This\ncommand clears\
    \ the homescreen. == Syntax ==\u2019\u2018\u2019ClrText\u2019\u2019\u2019 == Example\n\
    == ClrText \\[\\[Category:BASIC_Commands\\]\\]\u2019"
  timestamp: '2012-02-15T03:45:06Z'
title: ClrText
---

# ClrText

## Description

This command clears the homescreen.

## Syntax

**ClrText**

## Example

`ClrText`
