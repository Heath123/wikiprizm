---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= Bond_YLD( = == Description == This\ncommand\
    \ returns the yield based on specified conditions. == Syntax\n==\u2019\u2018\u2019\
    Bond_YLD(\u2019\u2019\u2019\u2018\u2019MM1\u2019\u2018,\u2019\u2018DD1\u2019\u2018\
    ,\u2019\u2018YYYY1\u2019\u2018,\u2019\u2018MM2\u2019\u2018,\u2019\u2018DD2\u2019\
    \u2018,\u2019\u2018YYYY2\u2019\u2018,\u2019\u2018RD\u2026\u2019"
  timestamp: '2012-02-16T00:25:06Z'
title: Bond_YLD(
---

# Bond_YLD(

## Description

This command returns the yield based on specified conditions.

## Syntax

**Bond_YLD(***MM1*,*DD1*,*YYYY1*,*MM2*,*DD2*,*YYYY2*,*RDV*,*CPN*,*PRC***)**

## Example

`Bond_YLD(04,21,1994,01,16,2000,12,12,100)`
