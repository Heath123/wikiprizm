---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= GeoPD( = == Description == This command\nreturns\
    \ the geometric distribution of event x and probability P.\nThis command follows\
    \ the formula P^x \\* (1-P)^(x-1) == Syntax\n==\u2019\u2018\u2019Ge\u2026\u2019"
  timestamp: '2012-02-15T12:41:29Z'
title: GeoPD(
---

# GeoPD(

## Description

This command returns the geometric distribution of event x and
probability P.

This command follows the formula P^x \* (1-P)^(x-1)

## Syntax

**GeoPD(***x*,*P*

x can be either value or list.

## Example

`GeoPD(5,.3`
